/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL_UTR;

import DTL_UTR.OrderUTR;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Fer Madrigal
 */
public class OrderRepositoryUTR {
    private static ArrayList<OrderUTR> getDataByQuery(String query) {

        ArrayList<OrderUTR> oOrders = new ArrayList<>();
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName("DESKTOP-KNFHHH4");
            ds.setInstanceName("SQLEXPRESS");
            ds.setPortNumber(1433);
            ds.setDatabaseName("ProjectUtrSsports");
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                OrderUTR order = new OrderUTR ();
                order.setOrderID(rs.getInt("OrderID"));
                order.setCreatedDate(rs.getDate("CreatedDate"));
                //oOrders.add(order);
                
                index++;
            }
            rs.close();
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oOrders;
    }
    
    private static void executeQuery(String query) {
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName("DESKTOP-KNFHHH4");
            ds.setInstanceName("SQLEXPRESS");
            ds.setPortNumber(1433);
            ds.setDatabaseName("ProjectUtrSsports");
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    
    public static ArrayList<OrderUTR> getAllPeople() {
        String query = "SELECT * "
                + "FROM Orders ";

        ArrayList<OrderUTR> oOrders = getDataByQuery(query);
        return oOrders;
    }
    
    public static ArrayList<OrderUTR> getOrderById(int id) {
        String query = "SELECT * "
                + "FROM Orders "
                + "WHERE OrderID = " + id;

        ArrayList<OrderUTR> oOrders = getDataByQuery(query);
        return oOrders;
    }
    
    public static void updateOrder(OrderUTR order) {
        String query = "  UPDATE [Orders]"
                + " SET [CreatedDate]  = '" + order.getCreatedDate() 
                + " WHERE [OrderID] = " + order.getOrderID();
        executeQuery(query);
    }
    
    public static void deleteOrderByID(int orderID) {
        String query = "  DELETE FROM [Orders]"
                + " WHERE [OrderID] = " + orderID;
        executeQuery(query);
    }
    
     public static void createOrder(OrderUTR order) {
        String query = " INSERT INTO Orders (CreatedDate)"
                + "VALUES ('"+ order.getCreatedDate() + "' )";
        executeQuery(query);
    }
}
