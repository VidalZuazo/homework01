/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL_UTR;


import DTL_UTR.PersonUTR;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Fer Madrigal
 */
public class PersonRepositoryUTR {
     private static ArrayList<PersonUTR> getDataByQuery(String query) {

        ArrayList<PersonUTR> oPeople = new ArrayList<>();
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName("DESKTOP-KNFHHH4");
            ds.setInstanceName("SQLEXPRESS");
            ds.setPortNumber(1433);
            ds.setDatabaseName("ProjectUtrSsports");
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                PersonUTR person = new PersonUTR();
                person.setPersonID(rs.getInt("PersonID"));
                person.setFirstName(rs.getString("FirstName"));
                person.setLastName(rs.getString("LastName"));
                person.setPassword(rs.getString("Password"));
                oPeople.add(person);

                index++;
            }
            rs.close();
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oPeople;
    }

    private static void executeQuery(String query) {
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName("DESKTOP-KNFHHH4");
            ds.setInstanceName("SQLEXPRESS");
            ds.setPortNumber(1433);
            ds.setDatabaseName("ProjectUtrSsports");
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<PersonUTR> getAllPeople() {
        String query = "SELECT * "
                + " FROM People ";

        ArrayList<PersonUTR> oPeople = getDataByQuery(query);
        return oPeople;
    }

    public static ArrayList<PersonUTR> getPersonById(int id) {
        String query = "SELECT * "
                + "FROM People "
                + "WHERE PersonID = " + id;

        ArrayList<PersonUTR> oPeople = getDataByQuery(query);
        return oPeople;
    }

    public static ArrayList<PersonUTR> getPersonByLoginData(String password, String firstName) {
        String query = "SELECT * "
                + " FROM People "
                + " WHERE FirstName = '" + firstName + "' AND Password = '" + password + "'";
        ArrayList<PersonUTR> oPeople = getDataByQuery(query);
        return oPeople;
    }

    public static void updatePerson(PersonUTR person) {
        String query = "  UPDATE [People]"
                + " SET [FirstName]  = '" + person.getFirstName() + "', [Password]  = '" + person.getPassword() + "' "
                + " WHERE [PersonID] = " + person.getPersonID();
        executeQuery(query);
    }

    public static void deleteUserByID(int personID) {
        String query = "  DELETE FROM [People]"
                + " WHERE [PersonID] = " + personID;
        executeQuery(query);
    }

    public static void createUser(PersonUTR person) {
        String query = " INSERT INTO People (FirstName, Password)"
                + "VALUES ('" + person.getFirstName() + "','" + person.getPassword() + "' )";
        executeQuery(query);
    }
}
