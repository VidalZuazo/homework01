/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL_UTR;

import DTL_UTR.OrderDetailUTR;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Fer Madrigal
 */
public class OrderDetailRepositoryUTR {
    private static ArrayList<OrderDetailUTR> getDataByQuery(String query) {

        ArrayList<OrderDetailUTR> oOrderDetails = new ArrayList<>();
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName("DESKTOP-KNFHHH4");
            ds.setInstanceName("SQLEXPRESS");
            ds.setPortNumber(1433);
            ds.setDatabaseName("ProjectUtrSsports");
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                OrderDetailUTR orderDetail = new OrderDetailUTR();
                orderDetail.setOrderDetailID(rs.getInt("OrderDetailID"));
                orderDetail.setOrderID(rs.getInt("OrderID"));
                orderDetail.setProductID(rs.getInt("ProductID"));
                orderDetail.setAmount(rs.getInt("Amount"));
                //oOrderDetails.add(orderDetail);

                index++;
            }
            rs.close();
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oOrderDetails;
    }

    private static void executeQuery(String query) {
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName("DESKTOP-KNFHHH4");
            ds.setInstanceName("SQLEXPRESS");
            ds.setPortNumber(1433);
            ds.setDatabaseName("ProjectUtrSsports");
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static ArrayList<OrderDetailUTR> getAllOrderDetail() {
        String query = "SELECT * "
                + "FROM OrderDetails ";

        ArrayList<OrderDetailUTR> oOrderDetails = getDataByQuery(query);
        return oOrderDetails;
    }

    public static ArrayList<OrderDetailUTR> getOrderDetailById(int id) {
        String query = "SELECT * "
                + "FROM OrderDetails "
                + "WHERE OrderDetailID = " + id;

        ArrayList<OrderDetailUTR> oOrderDetails = getDataByQuery(query);
        return oOrderDetails;
    }

    public static void updateOrderDetail(OrderDetailUTR orderDetail) {
        String query = "  UPDATE [OrderDetails]"
                + " SET [ProductID]  = '" + orderDetail.getProductID() 
                + " WHERE [OrderDetailID] = " + orderDetail.getOrderDetailID();
        executeQuery(query);
    }
    
    public static void deleteOrderDetailByID(int orderDetailID) {
        String query = "  DELETE FROM [OrderDetails]"
                + " WHERE [OrderDetailID] = " + orderDetailID;
        executeQuery(query);
    }
    
    public static void createOrderDetail(OrderDetailUTR orderDetail) {
        String query = " INSERT INTO OrderDetails (OrderID, ProductID, Amount)"
                + "VALUES ('" + orderDetail.getOrderID() + "','" + orderDetail.getProductID() + "','" + orderDetail.getAmount() + "' )";
        executeQuery(query);
    }
}
