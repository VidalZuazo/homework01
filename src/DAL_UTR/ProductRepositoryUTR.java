/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAL_UTR;

import DTL_UTR.ProductUTR;
import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Fer Madrigal
 */
public class ProductRepositoryUTR {
    private static ArrayList<ProductUTR> getDataByQuery(String query) {

        ArrayList<ProductUTR> oProducts = new ArrayList<>();
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName("DESKTOP-KNFHHH4");
            ds.setInstanceName("SQLEXPRESS");
            ds.setPortNumber(1433);
            ds.setDatabaseName("ProjectUtrSsports");
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            int index = 0;
            while (rs.next()) {
                ProductUTR product = new ProductUTR();
                product.setProductID(rs.getInt("ProductID"));
                product.setProduct_Name(rs.getString("Product_Name"));
                product.setBrandName(rs.getString("BrandName"));
                product.setModel(rs.getString("Model"));
                product.setPrice(rs.getFloat("Price"));
                product.setActive(rs.getBoolean("Active"));
                oProducts.add(product);

                index++;
            }
            rs.close();
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return oProducts;
    }

    private static boolean executeQuery(String query) {
        boolean succ = true;
        try {
            SQLServerDataSource ds = new SQLServerDataSource();
            ds.setIntegratedSecurity(true);
            ds.setServerName("DESKTOP-KNFHHH4");
            ds.setInstanceName("SQLEXPRESS");
            ds.setPortNumber(1433);
            ds.setDatabaseName("ProjectUtrSsports");
            //ds.setUser(DBConnection.DB_USER);
            //ds.setPassword(DBConnection.DB_PASSWORD);
            Connection con = ds.getConnection();
            Statement stmt = con.createStatement();
            stmt.executeUpdate(query);

            if (stmt != null) {

                try {
                    stmt.close();
                    //return sin problema

                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing Statment");
                    succ = false;
                }
            }
            if (con != null) {
                try {
                    con.close();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("Error closing connection");
                    succ = false;
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            succ = false;

        }
        return succ;
    }

    public static ArrayList<ProductUTR> getAllProducts() {
        int act = 1;
        String query = "SELECT * "
                + "FROM Products "
                + "WHERE Active =" + act;

        ArrayList<ProductUTR> oProducts = getDataByQuery(query);
        return oProducts;

    }

    public static ArrayList<ProductUTR> getProductById(int id) {
        String query = "SELECT * "
                + "FROM Products "
                + "WHERE ProductID = " + id;

        ArrayList<ProductUTR> oProducts = getDataByQuery(query);
        return oProducts;
    }

    public static boolean updateProduct(ProductUTR product) {
        String query = "  UPDATE [Products]"
                + " SET [Product_Name]  = '" + product.getProduct_Name() + "',  [Price] = " + product.getPrice() 
                + " WHERE [ProductID] = " + product.getProductID();
        return executeQuery(query);
    }

    public static boolean deleteProductByID(int productID) {   
        int delete = 0;
        String query = " UPDATE [Products]"
                + " SET [Active] = " +  delete 
                + " WHERE [ProductID] = " + productID;
        return executeQuery(query);

        
    }

    public static boolean createProduct(ProductUTR product) {
        
        String query = " INSERT INTO Products (Product_Name, BrandName,Model, Price, Active)"
                + "VALUES ('" + product.getProduct_Name() + "','" + product.getBrandName() + "','" + product.getModel() + "'," + product.getPrice() + ",'" + product.isActive() + "' )";
        return executeQuery(query);
    }
}
