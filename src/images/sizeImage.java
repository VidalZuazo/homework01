/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package images;

//Add Products
import static interfaces.AddProduct_SportsUTR.b_helpAdd;
import static interfaces.AddProduct_SportsUTR.b_iconAdd;
import static interfaces.AddProduct_SportsUTR.b_info_add;
import static interfaces.AddProduct_SportsUTR.l_back_add;
//Menu AdminProducts
import static interfaces.AdminProdcuts_SportsUTR.l_back_admiM;
import static interfaces.AdminProdcuts_SportsUTR.b_iconAdd_admi;
import static interfaces.AdminProdcuts_SportsUTR.b_iconUp_admi;
import static interfaces.AdminProdcuts_SportsUTR.b_iconD_admi;
import static interfaces.AdminProdcuts_SportsUTR.b_info_admi;
import static interfaces.AdminProdcuts_SportsUTR.b_help_admi;
//Card Payment
import static interfaces.CardPayment_SportsUTR.l_back_cardP;
//Cash Payment
import static interfaces.CashPayment_SportsUTR.l_back_cash;
//Help Add Products
import static interfaces.Help_AddP_SportsUTR.l_help_addP;
import static interfaces.Help_AddP_SportsUTR.l_back_helpAdd;
// Help Products Administration
import static interfaces.Help_AdmiP_SportsUTR.l_back_helpAP;
import static interfaces.Help_AdmiP_SportsUTR.l_img_HelpAP;
//Help Products
import static interfaces.Help_Products_SportsUTR.l_addT;
import static interfaces.Help_Products_SportsUTR.l_amountT;
import static interfaces.Help_Products_SportsUTR.l_back_helpP;
//Help Shopping Cart
import static interfaces.Help_Shopping_SportsUTR.l_addDS;
import static interfaces.Help_Shopping_SportsUTR.l_back_helpS;
import static interfaces.Help_Shopping_SportsUTR.l_waypay;
//Help Update Prodcuts
import static interfaces.Help_UpdateP_SportsUTR.l_back_helpUp;
import static interfaces.Help_UpdateP_SportsUTR.l_help_upP;
//Information
import static interfaces.Info_SportsUTR.l_back_info;
import static interfaces.Info_SportsUTR.l_logoUTR;
//Login 
import static interfaces.LogIn_SportsUTR.l_back_login;
import static interfaces.LogIn_SportsUTR.b_info_logIn;
import static interfaces.LogIn_SportsUTR.l_logo;
import static interfaces.LogIn_SportsUTR.l_pword;
import static interfaces.LogIn_SportsUTR.l_userIcon;
//Menu
import static interfaces.Menu_SportsUTR.l_back_menu;
import static interfaces.UpdateProduct_SportsUTR.b_helpUp;
//Products
import static interfaces.Products_SportsUTR.l_back_products;
import static interfaces.Products_SportsUTR.b_iconAdd_products;
//Shopping Cart
import static interfaces.ShoppingCart_SportsUTR.b_help_shop;
import static interfaces.ShoppingCart_SportsUTR.b_iconAdd_shop;
import static interfaces.ShoppingCart_SportsUTR.b_iconD_shop;
import static interfaces.ShoppingCart_SportsUTR.b_info_shop;
import static interfaces.ShoppingCart_SportsUTR.l_back_shop;
import static interfaces.ShoppingCart_SportsUTR.l_iconShop;
//Update Product
import static interfaces.UpdateProduct_SportsUTR.b_iconUp;
import static interfaces.UpdateProduct_SportsUTR.b_info_up;
import static interfaces.UpdateProduct_SportsUTR.l_back_up;
//Tools
import java.awt.Image;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author Pavilion
 */
public class sizeImage {
    
    //Start code of Log In
    
    public void back_login (){//Background of the Log In
        
        ImageIcon img = new ImageIcon("src/images/background_login.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_login.getWidth(), l_back_login.getHeight(), Image.SCALE_DEFAULT));
        l_back_login.setIcon(i);
        
    }
    
    public void logo (){
        
        ImageIcon img = new ImageIcon("src/images/logo.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_logo.getWidth(), l_logo.getHeight(), Image.SCALE_DEFAULT));
        l_logo.setIcon(i);
        
    }
    
    public void inf_login (){
        
        ImageIcon img = new ImageIcon("src/images/inf_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_info_logIn.getWidth(), b_info_logIn.getHeight(), Image.SCALE_DEFAULT));
        b_info_logIn.setIcon(i);
        
    }
    
    public void user_icon (){
        
        ImageIcon img = new ImageIcon("src/images/user_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_userIcon.getWidth(), l_userIcon.getHeight(), Image.SCALE_DEFAULT));
        l_userIcon.setIcon(i);
        
    }
    
    public void pword_icon (){
        
        ImageIcon img = new ImageIcon("src/images/password_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_pword.getWidth(), l_pword.getHeight(), Image.SCALE_DEFAULT));
        l_pword.setIcon(i);
        
    }
    
    //Finish code of Log In
    
    //Start code of Info JDialog
    
    public void back_info (){//Background of the Info
        
        ImageIcon img = new ImageIcon("src/images/background_extra.jpeg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_info.getWidth(), l_back_info.getHeight(), Image.SCALE_DEFAULT));
        l_back_info.setIcon(i);
        
    }
    
    public void logoUTR (){
        
        ImageIcon img = new ImageIcon("src/images/utr.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_logoUTR.getWidth(), l_logoUTR.getHeight(), Image.SCALE_DEFAULT));
        l_logoUTR.setIcon(i);
        
    }
    
    //Finish code of Info JDialog
    
    //Start code of HelpAdd JDialog
    
    public void back_helpAdd (){//Background of the Help
        
        ImageIcon img = new ImageIcon("src/images/background_extra.jpeg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_helpAdd.getWidth(), l_back_helpAdd.getHeight(), Image.SCALE_DEFAULT));
        l_back_helpAdd.setIcon(i);
        
    }
    
    public void img_helpAdd (){
        
        ImageIcon img = new ImageIcon("src/images/addP.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_help_addP.getWidth(), l_help_addP.getHeight(), Image.SCALE_DEFAULT));
        l_help_addP.setIcon(i);
        
    }
    
    //Finish code of HelpAdd JDialog
    
    //Start code of HelpUpdate JDialog
    
    public void back_helpUp (){//Background of the Help
        
        ImageIcon img = new ImageIcon("src/images/background_extra.jpeg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_helpUp.getWidth(), l_back_helpUp.getHeight(), Image.SCALE_DEFAULT));
        l_back_helpUp.setIcon(i);
        
    }
    
    public void img_helpUp (){
        
        ImageIcon img = new ImageIcon("src/images/updateP.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_help_upP.getWidth(), l_help_upP.getHeight(), Image.SCALE_DEFAULT));
        l_help_upP.setIcon(i);
        
    }
    
    //Finish code of HelpUpdate JDialog
    
    //Start code of Menu Sports
    
    public void back_menu (){//Background of the Menu Sports
        
        ImageIcon img = new ImageIcon("src/images/background_menu.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_menu.getWidth(), l_back_menu.getHeight(), Image.SCALE_DEFAULT));
        l_back_menu.setIcon(i);
        
    }
    
    //Finish code of Menu Sports
    
    //Start code of AdminProdcuts
    
    public void back_menuAdmin (){//Background of the Menu AdminProducts
        
        ImageIcon img = new ImageIcon("src/images/background_menu.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_admiM.getWidth(), l_back_admiM.getHeight(), Image.SCALE_DEFAULT));
        l_back_admiM.setIcon(i);
        
    }
    
    public void icon_addAdmi (){
        
        ImageIcon img = new ImageIcon("src/images/add.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_iconAdd_admi.getWidth(), b_iconAdd_admi.getHeight(), Image.SCALE_DEFAULT));
        b_iconAdd_admi.setIcon(i);
        
    }
    
    public void icon_upAdmi (){
        
        ImageIcon img = new ImageIcon("src/images/update.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_iconUp_admi.getWidth(), b_iconUp_admi.getHeight(), Image.SCALE_DEFAULT));
        b_iconUp_admi.setIcon(i);
        
    }
    
    public void icon_deleteAdmi (){
        
        ImageIcon img = new ImageIcon("src/images/delete.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_iconD_admi.getWidth(), b_iconD_admi.getHeight(), Image.SCALE_DEFAULT));
        b_iconD_admi.setIcon(i);
        
    }
    
    public void help_admi (){
        
        ImageIcon img = new ImageIcon("src/images/help_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_help_admi.getWidth(), b_help_admi.getHeight(), Image.SCALE_DEFAULT));
        b_help_admi.setIcon(i);
        
    }
    
    public void inf_admi (){
        
        ImageIcon img = new ImageIcon("src/images/inf_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_info_admi.getWidth(), b_info_admi.getHeight(), Image.SCALE_DEFAULT));
        b_info_admi.setIcon(i);
        
    }
    
    //Finish code of AdminProdcuts
    
    //Start code of AddProducts
    
    public void back_addProducts (){
        
        ImageIcon img = new ImageIcon("src/images/background.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_add.getWidth(), l_back_add.getHeight(), Image.SCALE_DEFAULT));
        l_back_add.setIcon(i);
        
    }
    
    public void icon_Add (){
        
        ImageIcon img = new ImageIcon("src/images/add.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_iconAdd.getWidth(), b_iconAdd.getHeight(), Image.SCALE_DEFAULT));
        b_iconAdd.setIcon(i);
        
    }
    
    public void inf_addProducts (){
        
        ImageIcon img = new ImageIcon("src/images/inf_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_info_add.getWidth(), b_info_add.getHeight(), Image.SCALE_DEFAULT));
        b_info_add.setIcon(i);
        
    }
    
    public void help_addProducts (){
        
        ImageIcon img = new ImageIcon("src/images/help_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_helpAdd.getWidth(), b_helpAdd.getHeight(), Image.SCALE_DEFAULT));
        b_helpAdd.setIcon(i);
        
    }
    
    //Finish code of AddProducts
    
    //Start code of UpdateProducts
    
    public void back_updateProducts (){
        
        ImageIcon img = new ImageIcon("src/images/background.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_up.getWidth(), l_back_up.getHeight(), Image.SCALE_DEFAULT));
        l_back_up.setIcon(i);
        
    }
    
    public void icon_Up (){
        
        ImageIcon img = new ImageIcon("src/images/update.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_iconUp.getWidth(), b_iconUp.getHeight(), Image.SCALE_DEFAULT));
        b_iconUp.setIcon(i);
        
    }
    
    public void inf_upProducts (){
        
        ImageIcon img = new ImageIcon("src/images/inf_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_info_up.getWidth(), b_info_up.getHeight(), Image.SCALE_DEFAULT));
        b_info_up.setIcon(i);
        
    }
    
    public void help_upProducts (){
        
        ImageIcon img = new ImageIcon("src/images/help_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_helpUp.getWidth(), b_helpUp.getHeight(), Image.SCALE_DEFAULT));
        b_helpUp.setIcon(i);
        
    }
    
    //Finish code of Updateproducts
    
    //Start code of Shopping Cart
    
    public void back_shop (){
        
        ImageIcon img = new ImageIcon("src/images/background.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_shop.getWidth(), l_back_shop.getHeight(), Image.SCALE_DEFAULT));
        l_back_shop.setIcon(i);
        
    }
    
    public void icon_Add_Shop (){
        
        ImageIcon img = new ImageIcon("src/images/add.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_iconAdd_shop.getWidth(), b_iconAdd_shop.getHeight(), Image.SCALE_DEFAULT));
        b_iconAdd_shop.setIcon(i);
        
    }
    
    public void icon_delete_shop (){
        
        ImageIcon img = new ImageIcon("src/images/delete.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_iconD_shop.getWidth(), b_iconD_shop.getHeight(), Image.SCALE_DEFAULT));
        b_iconD_shop.setIcon(i);
        
    }
    
    public void inf_shop (){
        
        ImageIcon img = new ImageIcon("src/images/inf_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_info_shop.getWidth(), b_info_shop.getHeight(), Image.SCALE_DEFAULT));
        b_info_shop.setIcon(i);
        
    }
    
    public void help_shop (){
        
        ImageIcon img = new ImageIcon("src/images/help_icon.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_help_shop.getWidth(), b_help_shop.getHeight(), Image.SCALE_DEFAULT));
        b_help_shop.setIcon(i);
        
    }
    
    public void icon_shop (){
        
        ImageIcon img = new ImageIcon("src/images/shopping.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_iconShop.getWidth(), l_iconShop.getHeight(), Image.SCALE_DEFAULT));
        l_iconShop.setIcon(i);
        
    }
    
    //Finish code of Shopping Cart
    
    //Start code of prodcuts
    
    public void back_products (){
        
        ImageIcon img = new ImageIcon("src/images/background_menu.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_products.getWidth(), l_back_products.getHeight(), Image.SCALE_DEFAULT));
        l_back_products.setIcon(i);
        
    }
    
    public void icon_Add_Products (){
        
        ImageIcon img = new ImageIcon("src/images/add.png");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(b_iconAdd_products.getWidth(), b_iconAdd_products.getHeight(), Image.SCALE_DEFAULT));
        b_iconAdd_products.setIcon(i);
        
    }
    
    //Finish code of products
    
    //Start code of Card Payment
    
    public void back_cardP (){
        
        ImageIcon img = new ImageIcon("src/images/background_menu.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_cardP.getWidth(), l_back_cardP.getHeight(), Image.SCALE_DEFAULT));
        l_back_cardP.setIcon(i);
        
    }
    
    //Finish code of Card Payment
    
    //Start code of Cash Payment
    
    public void back_cashP (){
        
        ImageIcon img = new ImageIcon("src/images/background_menu.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_cash.getWidth(), l_back_cash.getHeight(), Image.SCALE_DEFAULT));
        l_back_cash.setIcon(i);
        
    }
    
    //Finish code of Cash Payment
    
    //Start code of Help Products
    
    public void back_helpP (){
        
        ImageIcon img = new ImageIcon("src/images/background_extra.jpeg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_helpP.getWidth(), l_back_helpP.getHeight(), Image.SCALE_DEFAULT));
        l_back_helpP.setIcon(i);
        
    }
    
    public void img_addT (){
        
        ImageIcon img = new ImageIcon("src/images/add_ticket.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_addT.getWidth(), l_addT.getHeight(), Image.SCALE_DEFAULT));
        l_addT.setIcon(i);
        
    }
    
    public void img_amountT (){
        
        ImageIcon img = new ImageIcon("src/images/amount_products.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_amountT.getWidth(), l_amountT.getHeight(), Image.SCALE_DEFAULT));
        l_amountT.setIcon(i);
        
    }
    
    //Finish code of Help Products
    
    //Start code of Shopping Cart
    
    public void back_helpS (){
        
        ImageIcon img = new ImageIcon("src/images/background_extra.jpeg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_helpS.getWidth(), l_back_helpS.getHeight(), Image.SCALE_DEFAULT));
        l_back_helpS.setIcon(i);
        
    }
    
    public void img_addDS (){
        
        ImageIcon img = new ImageIcon("src/images/add_del_shop.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_addDS.getWidth(), l_addDS.getHeight(), Image.SCALE_DEFAULT));
        l_addDS.setIcon(i);
        
    }
    
    public void img_waypay (){
        
        ImageIcon img = new ImageIcon("src/images/pay_shop.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_waypay.getWidth(), l_waypay.getHeight(), Image.SCALE_DEFAULT));
        l_waypay.setIcon(i);
        
    }
    
    //Finish code of Help Shopping Cart
    
    //Start code of Help Products Administration
    
    public void back_helpAP (){
        
        ImageIcon img = new ImageIcon("src/images/background_extra.jpeg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_back_helpAP.getWidth(), l_back_helpAP.getHeight(), Image.SCALE_DEFAULT));
        l_back_helpAP.setIcon(i);
        
    }
    
    public void img_add_up_d (){
        
        ImageIcon img = new ImageIcon("src/images/add_up_d.jpg");
        Icon i = new ImageIcon(img.getImage().getScaledInstance(l_img_HelpAP.getWidth(), l_img_HelpAP.getHeight(), Image.SCALE_DEFAULT));
        l_img_HelpAP.setIcon(i);
        
    }
    
    //Finish code of Help Products Administration
    
}
