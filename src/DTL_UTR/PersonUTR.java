/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL_UTR;

/**
 *
 * @author Fer Madrigal
 */
public class PersonUTR {
    private int PersonID;
    private String FirstName;
    private String LastName;
    private String Password;
    
    public PersonUTR(int _personID, String _firstName, String _lastName, String _password) {
        this.PersonID = _personID;
        this.FirstName = _firstName;
        this.LastName = _lastName;
        this.Password = _password;
    }
    
    public PersonUTR(){
        
    }

    public int getPersonID() {
        return PersonID;
    }

    public void setPersonID(int PersonID) {
        this.PersonID = PersonID;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String FirstName) {
        this.FirstName = FirstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String LastName) {
        this.LastName = LastName;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    
}
