/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL_UTR;

/**
 *
 * @author Fer Madrigal
 */
public class OrderDetailUTR {
     private int OrderDetailID;
    private int OrderID;
    private int ProductID;
    private int Amount;

    public OrderDetailUTR(int _orderDetailID, int _orderID, int _productID, int _amount) {
        this.OrderDetailID = _orderDetailID;
        this.OrderID = _orderID;
        this.ProductID = _productID;
        this.Amount = _amount;
    }
    
    public OrderDetailUTR (){
        
    }

    public int getOrderDetailID() {
        return OrderDetailID;
    }

    public void setOrderDetailID(int OrderDetailID) {
        this.OrderDetailID = OrderDetailID;
    }

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int OrderID) {
        this.OrderID = OrderID;
    }

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int ProductID) {
        this.ProductID = ProductID;
    }

    public int getAmount() {
        return Amount;
    }

    public void setAmount(int Amount) {
        this.Amount = Amount;
    }
    
}
