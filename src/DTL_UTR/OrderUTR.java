/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL_UTR;


import java.util.Date;

/**
 *
 * @author Fer Madrigal
 */
public class OrderUTR {
    private int OrderID;
    private Date CreatedDate;

    public OrderUTR(int _orderID, Date _createdDate) {
        this.OrderID = _orderID;
        this.CreatedDate = _createdDate;
    }
    
    public OrderUTR (){
        
    }

    public int getOrderID() {
        return OrderID;
    }

    public void setOrderID(int OrderID) {
        this.OrderID = OrderID;
    }

    public Date getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(Date CreatedDate) {
        this.CreatedDate = CreatedDate;
    }
    
    
}
