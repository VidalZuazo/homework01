/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTL_UTR;

/**
 *
 * @author Fer Madrigal
 */
public class ProductUTR {
    private int ProductID;
    private String Product_Name;
    private String BrandName;
    private String Model;
    private float Price;
    private boolean Active;

    public ProductUTR(int _productID, String _name, String _brandName,String _model, float _price, boolean _active) {
        this.ProductID = _productID;
        this.Product_Name = _name;
        this.BrandName = _brandName;
        this.Model=_model;
        this.Price = _price;
        this.Active = _active;
    }
    
    public ProductUTR (){
        
    }

    public int getProductID() {
        return ProductID;
    }

    public void setProductID(int ProductID) {
        this.ProductID = ProductID;
    }

    public String getProduct_Name() {
        return Product_Name;
    }

    public void setProduct_Name(String Product_Name) {
        this.Product_Name = Product_Name;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String BrandName) {
        this.BrandName = BrandName;
    }

    public String getModel() {
        return Model;
    }

    public void setModel(String Model) {
        this.Model = Model;
    }

    public float getPrice() {
        return Price;
    }

    public void setPrice(float Price) {
        this.Price = Price;
    }

    public boolean isActive() {
        return Active;
    }

    public void setActive(boolean Active) {
        this.Active = Active;
    }

   
    
    

}
